﻿let $win = $(window);
let map=null;
const sv = new google.maps.StreetViewService();
let gameMode = "withLinksOnly";
const gameButtons = ["startGame","startingNewGame","guessGeoPosition","solveGame"];
let newGamePositionFound=false;
let withLinksOnly=false;
let gameLocation;
let gameLocationMarker;
let solvedMapConnector;
const markers=[];
//
function initialize() {
  let mapProp = {
    center:new google.maps.LatLng(48.994249,12.190451),
    zoom:10,
    mapTypeId:google.maps.MapTypeId.HYBRID
  };
  map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
  resizeMap();
  showGameButton("startGame");
  setUserHint("");
}
function setUserHint(hint) {
  const elHint = $("#userHint")[0];
  const elContent = $("#userHintContent")[0];
  elContent.innerText = hint;
  elHint.style.visibility = (hint) ? "visible" : "hidden";
}
function showGameButton(whichId) {
  gameButtons.forEach(btnId => {
    const btn = $("#"+btnId)[0];
    btn.style.visibility = (btnId == whichId) ? "visible" : "hidden";
  });
}
function showHelpInfo(yes) {
  const btn = $("#helpInfo")[0];
  btn.style.visibility= yes ? "visible" : "hidden";
}
function resizeMap() {
  $("#googleMap").width($win.width()*0.99);
  $("#googleMap").height($win.height()*0.98);
}
function displayMap(yes) {
  map.streetView.setVisible(!yes);
}
function createRandomGeoPosition() {
  const a = -1 + 2 * Math.random();
  const b = Math.asin(a)/1.570796326795;
  const loc={ lat: 90*b, lng: -180+360*Math.random() };
  //console.log("a="+a+", b="+b+", loc="+loc);
  return loc;
}
function testRandomGeoPositions() {
  markers.forEach(element => {
    element.setMap(null);
  });
  markers.length=0;
  for (let i=0;i<1000;i++) {
    const marker = new google.maps.Marker({
      position: createRandomGeoPosition(),
      map,
    });
    markers.push(marker);
  }
}
function startNewGame() {
  withLinksOnly=(gameMode == "withLinksOnly");
  solvedMapConnector?.setMap(null);
  newGamePositionFound=false;
  showGameButton("startingNewGame");
  findNewGamePosition();
}
function findNewGamePosition() {
  // Search new random position with street view data.
  const loc=createRandomGeoPosition();
  sv.getPanorama(
    {
      location: loc,
      radius: 50000,
      source: google.maps.StreetViewSource.DEFAULT,
    })
    .then(processSVData)
    .catch((e) => {
      setTimeout(() => {
        if (!newGamePositionFound) {
          findNewGamePosition();
        }
      },100);
    });
}
function processSVData({ data }) {
  if (withLinksOnly && data.links.length<1) {
    findNewGamePosition();
    return;
  }
  if (!withLinksOnly && data.links.length>=1) {
    findNewGamePosition();
    return;
  }
  newGamePositionFound=true;
  setUserHint("Finde heraus, wo du bist!");
  gameLocation = data.location;
  gameLocationMarker = new google.maps.Marker({
    position: gameLocation.latLng,
    map,
    title: gameLocation.shortDescription,
    icon: "symbols/PositionTrue.svg",
  });
  gameLocationMarker.setMap(null);
  markers.forEach(element => {
    element.setMap(null);
  });
  markers.length=0;
  markers.push(gameLocationMarker);
  map.streetView.setOptions(
    {
      addressControl: false,
      motionTracking: false,
    }
  );
  map.streetView.setPosition(gameLocation.latLng);
  map.streetView.setPov({
    heading: Math.random() * 360,
    pitch: 0,
  });
  map.streetView.setZoom(1);
  displayMap(false);
  showGameButton("");
  setTimeout(()=>showGameButton("guessGeoPosition"),3000);
}
function guessGeoPosition() {
  displayMap(true);
  map.setZoom(2);
  $("#guessMapCenterSymbol")[0].style.visibility="visible";
  resizeMap();
  setUserHint("Bring deine Lösung in die Mitte!");
  showGameButton("solveGame");
}
function solveGame() {
  $("#guessMapCenterSymbol")[0].style.visibility="hidden";
  displayMap(true);
  const locGuess = map.getCenter();
  const marker = new google.maps.Marker({
    position: locGuess,
    map,
    icon: "symbols/PositionGuess.svg",
  });
  markers.push(marker);
  const bounds = getLatLngBounds([locGuess,gameLocation.latLng]);
  map.fitBounds(bounds,100);
  gameLocationMarker.setMap(map);
  solvedMapConnector = createMapConnector(map, locGuess, gameLocation.latLng)
  setUserHint("Entfernung: "+(solvedMapConnector.distance/1000).toFixed(0)+" km");
  resizeMap();
  showGameButton("startGame");
}
