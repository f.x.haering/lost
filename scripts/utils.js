﻿
function getLatLngBounds(aLatLng) {
  let latMin, latMax;
  let lngMin, lngMax;
  aLatLng.forEach((latLng, index, array) => {
    let lat = isNaN(latLng.lat)?latLng.lat():latLng.lat;
    let lng = isNaN(latLng.lng)?latLng.lng():latLng.lng;
    if (!latMin) {
      latMin = latMax = lat;
      lngMin = lngMax = lng;
    } else {
      latMin = Math.min(latMin, lat);
      latMax = Math.max(latMax, lat);
      lngMin = Math.min(lngMin, lng);
      lngMax = Math.max(lngMax, lng);
    }
  });
  return new google.maps.LatLngBounds(new google.maps.LatLng(latMin, lngMin), new google.maps.LatLng(latMax, lngMax));
}
function createMapConnector(map, latLngA, latLngB) {
  let mapConnector = new google.maps.Polyline({
    map: map,
    geodesic: false,
    strokeColor: '#50D020',
    strokeOpacity: 0.8,
    strokeWeight: 4,
    icons: [
      {
        icon: {
          path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
        },
        offset: '100%',
      },
      {
        icon: {
          path: google.maps.SymbolPath.FORWARD_OPEN_ARROW,
        },
        offset: '66%',
        label: "TSV 1860",
      },
      {
        icon: {
          path: google.maps.SymbolPath.FORWARD_OPEN_ARROW,
        },
        offset: '33%',
      },
      {
        icon: {
          path: google.maps.SymbolPath.CIRCLE,
        },
        offset: '0%',
      },
    ],
    path: [latLngA, latLngB ],
  });
  mapConnector.distance=google.maps.geometry.spherical.computeLength(mapConnector.getPath());
  return mapConnector;
}
